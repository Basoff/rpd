from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter.ttk import Combobox
from tkinter.ttk import Checkbutton
import docx

def filename_click():
    template_route = filedialog.askopenfilename(title='Выберите файл с шаблоном таблицы',
                                      filetypes=(("Word documents (.doc, .docx)", "*.doc, *.docx"),("all files", "*.*")))
    template_filename.configure(text=template_route.split("/")[-1])

data = {}

def nextform():
    print(discipline_name.get(), '\n', laboriousness_value.get())
    data['file_route'] = template_route
    data['discipline_name'] = discipline_name.get()
    data['laboriousness_value'] = laboriousness_value.get()
    data['course_hours'] = hours_value.get()
    data['exam_hours'] = exam_hours_value.get()
    data['exam'] = exam.get()

#Общие настройки окна
form1 = Tk()
form1.title('Форма 1')
form1.geometry('450x250')

#Загрузка файла с шаблоном
template_route = filedialog.askopenfilename(title='Выберите файл с шаблоном таблицы',
                                           filetypes=(("Word documents (.doc, .docx)", "*.doc, *.docx"),("all files", "*.*"))
                                            )
filename_lbl = Label(form1, text='Образец оформления:', justify=LEFT, relief=GROOVE)
filename_lbl.place(x=10, y=10, width=220, height=20)
template_filename = Button(form1, text=template_route.split("/")[-1], justify=LEFT, relief=RAISED,
                           command=filename_click)
template_filename.place(x=240, y=10, width=200, height=20)

#Название дисциплины
discipline_name = StringVar()
discipline_name_lbl = Label(form1, text='Введите название дисциплины:', justify=LEFT, relief=GROOVE)
discipline_name_lbl.place(x=10, y=40, width=220, height=20)
discipline_name_entry = Entry(form1, textvariable=discipline_name, justify=LEFT)
discipline_name_entry.place (x=240, y=40, width=200, height=20)

#Трудоёмкость
laboriousness_value = IntVar()
laboriousness_lbl = Label(form1, text='Введите трудоёмкость дисциплины:', justify=LEFT, relief=GROOVE)
laboriousness_lbl.place(x=10, y=70, width=220, height=20)
laboriousness_entry = Spinbox(form1, textvariable=laboriousness_value, from_=0, to=100)
laboriousness_entry.place (x=240, y=70, width=200, height=20)

#Кол-во часов в курсе
hours_value = IntVar()
hours_lbl = Label(form1, text='Введите количество часов:', justify=LEFT, relief=GROOVE)
hours_lbl.place(x=10, y=100, width=220, height=20)
hours_entry = Spinbox(form1, textvariable = hours_value, from_=0, to=100)
hours_entry.place (x=240, y=100, width=200, height=20)

#Кол-во часов на экзамены
exam_hours_value = IntVar()
exam_hours_lbl = Label(form1, text='Введите количество часов на экзамены:', justify=LEFT, relief=GROOVE)
exam_hours_lbl.place(x=10, y=130, width=220, height=20)
exam_hours_entry = Spinbox(form1, textvariable = exam_hours_value, from_=0, to=100)
exam_hours_entry.place (x=240, y=130, width=200, height=20)

#Форма итоговой аттестации
exam = StringVar()
exam_lbl = Label(form1, text='Введите форму итоговой аттестации:', justify=LEFT, relief=GROOVE)
exam_lbl.place(x=10, y=160, width=220, height=20)
exam_entry = Combobox(form1, textvariable = exam)
exam_entry.place (x=240, y=160, width=200, height=20)
exam_entry['values']=('Зачёт','Экзамен')

#Кнопка выхода
exit_btn = Button(form1, text='Выход', command=lambda: form1.destroy())
exit_btn.place (x=10, y=220, height=20, width=50)

#Кнопка "Продолжить"
next_btn = Button(form1, text='Дальше', command=nextform)
next_btn.place (x=390, y=220, height=20, width=50)

form1.mainloop()
